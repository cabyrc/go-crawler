Go Crawler Example
==================

This application is an example of creating multi-threaded web crawler microservice in golang.
The task description can be found [here](https://gist.github.com/denisvmedia/29ccc84271c8b1c3bc29f548ad74a4a7).

## Compilation

### Prerequisites

This project requires Go 1.13 or later. If you haven't installed Go yet, use the binary installers available from the [official Go website](https://getgb.io/docs/install/).

### Compilation

- Build the binary:

```bash
go build
```

## Configuration

By default the app binds to `:8080`, and uses 10 item pool for batches and each batch uses 10 pools for its workers.
You can override the defaults using environment variables:

```bash
#!/bin/sh
CRAWLER_BIND_ADDRESS=":8081" CRAWLER_BATCH_POOL_SIZE="5" CRAWLER_WORKER_POOL_SIZE="5" ./go-crawler
```

## Usage

Run the following command:

```bash
./go-crawler
```

[Here](https://gist.github.com/denisvmedia/d7fca7dfbb61e4273e05821bbd92dc20) you can find a sample script how to use the daemon.

Please note that I use a free instance on herokuapp.com, so it is turned off by default, but will start with the first request (so it may take a bit more time to connect).

## Running tests

Run the following command:

```bash
go test -race ./...
```

Normally, you should see the following output:

```
?       go-crawler      [no test files]
ok      go-crawler/pkg/crawler  4.064s
```

If you see anything else, it means that the tests have failed.
