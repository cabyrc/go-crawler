package main

import (
	"go-crawler/pkg/crawler"
	"log"
	"net/http"
	"os"
	"strconv"
)

// Read command-line arguments and try to load the config file
func main() {
	batchPoolSizeStr := os.Getenv("CRAWLER_BATCH_POOL_SIZE")
	batchPoolSize, err := strconv.ParseInt(batchPoolSizeStr, 10, 32)
	if err != nil || batchPoolSize <= 0 {
		batchPoolSize = 10
	}

	workerPoolSizeStr := os.Getenv("CRAWLER_WORKER_POOL_SIZE")
	workerPoolSize, err := strconv.ParseInt(workerPoolSizeStr, 10, 32)
	if err != nil || workerPoolSize <= 0 {
		workerPoolSize = 10
	}

	cr := crawler.NewCrawler(int(batchPoolSize), int(workerPoolSize), crawler.GetHttpClient())
	http.HandleFunc("/tasks/", cr.GetHttpHandler())

	bindAddress := os.Getenv("CRAWLER_BIND_ADDRESS")
	if bindAddress == "" {
		bindAddress = ":8080"
	}
	log.Println("Batch Pool Size =", batchPoolSize)
	log.Println("Worker Pool Size =", workerPoolSize)
	log.Println("Binding", bindAddress)

	// Normally the program will never stop and as such log.Fatal(...) will never be called
	// but if bind fails log.Fatal will display the error and call os.Exit(1)
	log.Fatal(http.ListenAndServe(bindAddress, nil))
}
