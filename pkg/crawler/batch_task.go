package crawler

import (
	"log"
)

type BatchTaskFinishedCallback func(uint64, []CrawlerTaskResult)

type BatchTaskMapItem struct {
	TaskStatus int
	Results    []CrawlerTaskResult
}

type BatchTaskMap map[uint64]BatchTaskMapItem

type BatchTask struct {
	taskId         uint64
	urls           []string
	finishedFn     BatchTaskFinishedCallback
	httpClient     HttpClient
	workerPoolSize int
}

func (ct *BatchTask) Execute() {
	log.Println("Running task", ct.taskId)

	pool := NewPool(ct.workerPoolSize)

	crawlerResults := make([]CrawlerTaskResult, len(ct.urls))

	for idx, url := range ct.urls {
		i := idx // local var copy to use in go-routine
		pool.Exec(NewCrawlerTask(url, func(ctr *CrawlerTaskResult) {
			crawlerResults[i] = *ctr
		}, ct.httpClient))
	}

	pool.Close()
	pool.Wait()

	log.Println("Finishing task", ct.taskId)
	ct.finishedFn(ct.taskId, crawlerResults)
}

func NewBatchTask(taskId uint64, urls []string, finishedFn BatchTaskFinishedCallback, httpClient HttpClient) *BatchTask {
	return &BatchTask{
		taskId:         taskId,
		urls:           urls,
		finishedFn:     finishedFn,
		httpClient:     httpClient,
		workerPoolSize: 10,
	}
}
