package crawler

import (
	"crypto/tls"
	"net"
	"net/http"
	"time"
)

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

func GetHttpClient() HttpClient {
	// transport is set to ignore invalid certificates (TODO: probably should be in dev-mode only)
	tr := &http.Transport{
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},
		MaxIdleConnsPerHost: 1024,
		Proxy:               http.ProxyFromEnvironment,
		Dial: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: time.Minute,
		}).Dial,
	}

	// init client
	c := &http.Client{
		Transport: tr,
		Timeout:   time.Duration(100) * time.Second,
	}

	return c
}
