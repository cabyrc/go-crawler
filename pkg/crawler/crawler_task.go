package crawler

import (
	"compress/gzip"
	"io"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"time"
)

type CrawlerTaskResult struct {
	StatusCode      int
	Url             string
	PageTitle       string
	MetaDescription string
	MetaKeywords    string
	OgImage         string
}

func (ctr CrawlerTaskResult) ToSlice() []string {
	result := make([]string, 6)
	result[0] = strconv.Itoa(ctr.StatusCode)
	result[1] = ctr.Url
	result[2] = ctr.PageTitle
	result[3] = ctr.MetaDescription
	result[4] = ctr.MetaKeywords
	result[5] = ctr.OgImage

	return result
}

type CrawlerTaskFinishedCallback func(*CrawlerTaskResult)

type CrawlerTask struct {
	httpClient HttpClient
	url        string
	finishedFn CrawlerTaskFinishedCallback
}

func (ct *CrawlerTask) Execute() {
	log.Println("parsing", ct.url)

	ctr := &CrawlerTaskResult{
		StatusCode: -1,
		Url:        ct.url,
	}
	defer ct.finishedFn(ctr)

	// basic url check
	re := regexp.MustCompile("^https?://.*\\..*")
	valid := re.MatchString(ct.url)
	if !valid {
		log.Printf("Url %s does not pass basic format check", ct.url)
		return
	}

	// extended url check against RFC 3986
	// this still will allow some bullshit like http://-invalid-host/something.php, but this will fail on the network level
	// and still, ideally there should be a check that we do not connect to ourselves (nor to our internal servers),
	// but this is an extra feature that is beyond the task scope
	_, err := url.ParseRequestURI(ct.url)
	if err != nil {
		log.Printf("Url %s does not pass url.ParseRequestURI() check", ct.url)
		return
	}

	// We will also consider the url malformed, if url.Parse() fails
	urlData, err := url.Parse(ct.url)
	if err != nil {
		log.Printf("Url %s does not pass url.Parse() check", ct.url)
		return
	}

	for {
		if RateLimit(urlData.Host, time.Second) {
			break
		}
		log.Printf("Last connection to %s was less than a second ago, let's sleep for a second before the next attempt", urlData.Host)
		time.Sleep(time.Second)
	}

	req, err := http.NewRequest("GET", ct.url, nil)
	req.Header.Add("Accept-Encoding", "gzip")

	// Perform the request
	resp, err := ct.httpClient.Do(req)
	if err != nil {
		log.Println("Request to", ct.url, "failed:", err)

		// assume it was a network-level error (however, it can be a malform request as well)
		ctr.StatusCode = 0
		return
	}
	if resp.Body == nil {
		log.Println("No response body at", ct.url)

		// let's set it to 0 like a network error
		ctr.StatusCode = 0
		return
	}

	defer resp.Body.Close()

	var reader io.ReadCloser
	switch resp.Header.Get("Content-Encoding") {
	case "gzip":
		log.Println("Got gziped response, ungzipping")
		reader, err = gzip.NewReader(resp.Body)
		if err != nil {
			log.Println("Gunzipping response from", ct.url, "failed:", err)

			// let's set it to 0 like a network error
			ctr.StatusCode = 0
			return
		}
		defer reader.Close()
	default:
		reader = resp.Body
	}

	ctr.StatusCode = resp.StatusCode

	err = ParseBodyToResult(reader, ctr)
	if err != nil {
		log.Println("Parsing html body of", ct.url, "failed:", err)

		// let's set it to 0 like a network error as we don't have a better code
		ctr.StatusCode = 0
		return
	}
}

func NewCrawlerTask(url string, finishedFn CrawlerTaskFinishedCallback, httpClient HttpClient) *CrawlerTask {
	return &CrawlerTask{
		httpClient: httpClient,
		url:        url,
		finishedFn: finishedFn,
	}
}
