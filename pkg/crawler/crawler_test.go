package crawler

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	if m.DoFunc != nil {
		return m.DoFunc(req)
	}
	return &http.Response{}, nil
}

func TestCrawler_GetTask_Success(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusOK,
				Body: ioutil.NopCloser(bytes.NewBufferString(`
<html>
	<head>
		<title>go - sample page</title>
		<meta property="og:image" itemprop="image primaryImageOfPage" content="https://example.com/apple-touch-icon@2.png" />
		<meta name="description" content="Sample Go Description"/>
		<meta name="keywords" content="sample,go,keywords,go,here"/>
	</head>
	<body>
		Hello world!
    </body>
</html>

				`)),
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/",
		bytes.NewBufferString("http://google.com"))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	time.Sleep(time.Second)

	rr = httptest.NewRecorder()
	req, err = http.NewRequest("GET", "/tasks/1/", nil)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := "200,http://google.com,go - sample page,Sample Go Description,\"sample,go,keywords,go,here\",https://example.com/apple-touch-icon@2.png\n"
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestCrawler_GetTask_SuccessWithDelete(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusOK,
				Body: ioutil.NopCloser(bytes.NewBufferString(`
<html>
	<head>
		<title>go - sample page</title>
		<meta property="og:image" itemprop="image primaryImageOfPage" content="https://example.com/apple-touch-icon@2.png" />
		<meta name="description" content="Sample Go Description"/>
		<meta name="keywords" content="sample,go,keywords,go,here"/>
	</head>
	<body>
		Hello world!
    </body>
</html>

				`)),
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/",
		bytes.NewBufferString("http://google.com"))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	time.Sleep(time.Second)

	rr = httptest.NewRecorder()
	req, err = http.NewRequest("GET", "/tasks/1/?delete=1", nil)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := "200,http://google.com,go - sample page,Sample Go Description,\"sample,go,keywords,go,here\",https://example.com/apple-touch-icon@2.png\n"
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestCrawler_GetTask_SuccessEmptyBody(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusBadRequest,
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/",
		bytes.NewBufferString("http://google.com"))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	time.Sleep(time.Second)

	rr = httptest.NewRecorder()
	req, err = http.NewRequest("GET", "/tasks/1/", nil)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := "0,http://google.com,,,,\n"
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestCrawler_SubmitTask_Success(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusOK,
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/",
		bytes.NewBufferString("http://google.com\nhttp://microsoft.com\nhttps://bing.com\ninvalid://.domain123\ngarbage-123"))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `1`
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestCrawler_SubmitTask_FailureEmptyUrlList(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusOK,
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/",
		bytes.NewBufferString(""))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	// Check the response body is what we expect.
	expected := "url list can't be empty\n"
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestCrawler_SubmitTask_FailureNoBody(t *testing.T) {
	client := &MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			// do whatever you want
			return &http.Response{
				StatusCode: http.StatusOK,
			}, nil
		},
	}

	crawler := NewCrawler(1, 1, client)

	req, err := http.NewRequest("POST", "/tasks/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(crawler.GetHttpHandler())
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	// Check the response body is what we expect.
	expected := "can't read body\n"
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestMethodNotAllowedHandler(t *testing.T) {
	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("POST", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MethodNotAllowedHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusMethodNotAllowed)
	}

	// Check the response body is what we expect.
	expected := `Method not allowed`
	if rr.Body.String() != expected {
		t.Fatalf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
