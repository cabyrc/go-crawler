package crawler

import (
	"errors"
	"golang.org/x/net/html"
	"io"
	"log"
)

// Attempts to interpret a given val interface{} as string
func ParseString(val interface{}) (result string) {
	result, _ = val.(string)

	return result
}

// Attempts to interpret a given val interface{} as float64
func ParseFloat64(val interface{}) (result float64) {
	result, _ = val.(float64)

	return result
}

// Attempts to interpret a given val interface{} as float32
func ParseFloat32(val interface{}) (result float32) {
	result, _ = val.(float32)

	return result
}

// calls recover() and logs an error if there was an error recovered
func RecoverPanic() {
	if r := recover(); r != nil {
		log.Println("panic [recovered]:", r)
	}
}

func ExtractMetaProperty(t html.Token, prop string) (content string, ok bool) {
	for _, attr := range t.Attr {
		if attr.Key == "property" && attr.Val == prop {
			ok = true
		}

		if attr.Key == "content" {
			content = attr.Val
		}
	}

	return
}

func ExtractMetaName(t html.Token, prop string) (content string, ok bool) {
	for _, attr := range t.Attr {
		if attr.Key == "name" && attr.Val == prop {
			ok = true
		}

		if attr.Key == "content" {
			content = attr.Val
		}
	}

	return
}

func ParseBodyToResult(body io.Reader, ctr *CrawlerTaskResult) (err error) {
	z := html.NewTokenizer(body)

	titleFound := false

	for {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			return errors.New("invalid token")
		case html.StartTagToken, html.SelfClosingTagToken:
			t := z.Token()
			if t.Data == `body` {
				return
			}
			if t.Data == "title" {
				titleFound = true
			}
			if t.Data == "meta" {
				desc, ok := ExtractMetaName(t, "description")
				if ok {
					ctr.MetaDescription = desc
				}

				keywords, ok := ExtractMetaName(t, "keywords")
				if ok {
					ctr.MetaKeywords = keywords
				}

				ogImage, ok := ExtractMetaProperty(t, "og:image")
				if ok {
					ctr.OgImage = ogImage
				}
			}
		case html.TextToken:
			if titleFound {
				t := z.Token()
				ctr.PageTitle = t.Data
				titleFound = false
			}
		}
	}
}
