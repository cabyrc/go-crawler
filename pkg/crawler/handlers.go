package crawler

import (
	"net/http"
)

func MethodNotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	result := "Method not allowed"
	w.WriteHeader(http.StatusMethodNotAllowed)
	w.Write([]byte(result))
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	result := "Not found"
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(result))
}
