package crawler

import (
	"sync"
	"time"
)

type rateLimitStruct struct {
	data map[string]time.Time
	mu   sync.Mutex
}

var rateLimitVar = &rateLimitStruct{
	data: make(map[string]time.Time),
}

func RateLimit(host string, rateLimitTime time.Duration) bool {
	rateLimitVar.mu.Lock()
	defer rateLimitVar.mu.Unlock()

	val, ok := rateLimitVar.data[host]
	if !ok {
		// set time to now and exit (first call to this host)
		rateLimitVar.data[host] = time.Now()
		return true
	}

	duration := time.Since(val)
	if duration < rateLimitTime {
		// too soon, return false
		return false
	}

	// set time to now and exit (another call to this host)
	rateLimitVar.data[host] = time.Now()
	return true
}
