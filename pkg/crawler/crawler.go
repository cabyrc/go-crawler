package crawler

import (
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
)

type Crawler struct {
	taskCounter       uint64
	httpClient        HttpClient
	batchPool         *Pool
	batchTaskMap      BatchTaskMap
	batchTaskMapMutex sync.RWMutex
	workerPoolSize    int
}

func (c *Crawler) getCsv(results []CrawlerTaskResult) []byte {
	var buf bytes.Buffer
	writer := csv.NewWriter(&buf)

	for _, value := range results {
		err := writer.Write(value.ToSlice())
		if err != nil {
			log.Fatal("Cannot write csv data to memory", err)
		}
	}

	writer.Flush()

	return buf.Bytes()
}

func (c *Crawler) handleSubmitTask(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		log.Println("Missing body")
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	resultInt, err := c.submitTask(string(body))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	result := strconv.FormatUint(resultInt, 10)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write([]byte(result))
}

func (c *Crawler) handleGetTask(w http.ResponseWriter, r *http.Request) {
	re := regexp.MustCompile("^/tasks/(\\d+)/$")
	match := re.FindStringSubmatch(r.URL.Path)
	if len(match) < 2 {
		log.Println(match, r.URL.Path)
		http.Error(w, "Bad Request (task id not found)", http.StatusBadRequest)
		return
	}

	taskId, _ := strconv.ParseUint(match[1], 10, 64)
	if taskId <= 0 {
		log.Println(match, r.URL.Path)
		http.Error(w, "Bad Request (task id is incorrect)", http.StatusBadRequest)
		return
	}

	c.batchTaskMapMutex.RLock()
	item, ok := c.batchTaskMap[taskId]
	c.batchTaskMapMutex.RUnlock()
	if !ok {
		log.Println("Task not found", taskId)
		http.Error(w, "Task not found", http.StatusNotFound)
		return
	}

	if item.TaskStatus == 204 {
		w.WriteHeader(item.TaskStatus)
		log.Println("Task in progress", taskId)
		return
	}

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment;filename=%d.csv", taskId))
	w.Header().Set("Content-Type", "text/csv; charset=utf-8")
	w.WriteHeader(item.TaskStatus)
	w.Write(c.getCsv(item.Results))

	if r.URL.Query().Get("delete") == "1" {
		c.batchTaskMapMutex.Lock()
		delete(c.batchTaskMap, taskId)
		c.batchTaskMapMutex.Unlock()
	}
}

func (c *Crawler) GetHttpHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "POST":
			c.handleSubmitTask(w, r)
		case "GET":
			c.handleGetTask(w, r)
		default:
			MethodNotAllowedHandler(w, r)
		}

	}
}

func (c *Crawler) submitTask(urls string) (uint64, error) {
	urls = strings.TrimSpace(urls)

	if urls == "" {
		return 0, errors.New("url list can't be empty")
	}

	urlSlice := strings.Split(urls, "\n")

	taskId := atomic.AddUint64(&c.taskCounter, 1)
	log.Println("Inserting task", taskId)
	c.batchTaskMapMutex.Lock()
	c.batchTaskMap[taskId] = BatchTaskMapItem{
		TaskStatus: 204,
	}
	c.batchTaskMapMutex.Unlock()

	batchTask := NewBatchTask(taskId, urlSlice, func(taskId uint64, results []CrawlerTaskResult) {
		c.batchTaskMapMutex.Lock()
		c.batchTaskMap[taskId] = BatchTaskMapItem{
			TaskStatus: 200,
			Results:    results,
		}
		c.batchTaskMapMutex.Unlock()
	}, c.httpClient)
	batchTask.workerPoolSize = c.workerPoolSize
	c.batchPool.Exec(batchTask)

	return taskId, nil
}

func NewCrawler(batchPoolSize int, workerPoolSize int, httpClient HttpClient) *Crawler {
	return &Crawler{
		taskCounter:    0,
		httpClient:     httpClient,
		batchPool:      NewPool(batchPoolSize),
		batchTaskMap:   BatchTaskMap{},
		workerPoolSize: workerPoolSize,
	}
}
